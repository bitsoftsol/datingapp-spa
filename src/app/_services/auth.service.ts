import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '../../../node_modules/@angular/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from '../../../node_modules/rxjs';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = 'api/auth/';
  userToken: any;
  decodedTokenn: any;
  jwtHelper: JwtHelper = new JwtHelper();

constructor(private http: Http) { }
login(model: any) {
  return this.http.post(this.baseUrl + 'login', model, this.requestOptions()).pipe(map((response: Response) => {
    const user = response.json();
    if (user) {
      localStorage.setItem('token', user.tokenString);
      this.decodedTokenn = this.jwtHelper.decodeToken(user.tokenString);
      this.userToken = user.tokenString;
    }
  })).pipe(catchError(this.handleError));
}
register(model: any) {
  return this.http.post(this.baseUrl + 'register', model, this.requestOptions()).pipe(catchError(this.handleError));
}
loggedIn() {
  return tokenNotExpired('token');
}
private requestOptions() {
  const headers = new Headers({
    'Content-type': 'application/json'});
  return new RequestOptions({headers: headers});
}
private handleError(error: any) {
  const applicationError = error.headers.get('Application-Error');
  if (applicationError) {
    return throwError(applicationError);
  }
  const serverError = error.json();
  let modelStateErrors = '';
  if (serverError) {
    for (const key in serverError) {
      if (serverError[key]) {
        modelStateErrors += serverError[key] + '\n';
      }
    }
  }
  return throwError(
    modelStateErrors || 'Server Eroor'
  );
}
}
