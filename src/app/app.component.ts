import { Component, OnInit } from '@angular/core';
import { JwtHelper } from '../../node_modules/angular2-jwt';
import { AuthService } from './_services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'DatingApp';
  JwtHelper: JwtHelper = new JwtHelper();
  constructor(private authService: AuthService) {
  }
  ngOnInit() {
    const token = localStorage.getItem('token');
    if (token) {
      this.authService.decodedTokenn = this.JwtHelper.decodeToken(token);
    }
  }
}
