import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { error } from '../../../node_modules/protractor';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(public authService: AuthService, private alertify: AlertifyService) { }
  model: any = {};
  ngOnInit() {
  }
  login() {
    this.authService.login(this.model).subscribe(data => {
      this.alertify.success('Logged In Successfully');
    }, error => {
      this.alertify.error(error);
    });
  }
  logout() {
    this.authService.userToken = null;
    localStorage.removeItem('token');
    this.alertify.message('Logged Out');
  }
  loggedIn() {
    return this.authService.loggedIn();
  }
}
